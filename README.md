# Sorting Visualizer

Sorting Visualizer is a Python application that uses the Tkinter library to visualize various sorting algorithms. It allows users to generate data, choose a sorting algorithm, adjust the sorting speed, and visualize the sorting process in real-time.

## Features

- Five Sorting algorithms: Selection Sort, Bubble Sort, Insertion Sort, Merge Sort, Quick Sort.
- Three visualization types: Scatter plot, Bar plot, Stem plot.
- Adjustable size and speed of sorting.

## Prerequisites

Before running this application, you must have Python (3.x) installed on your computer. You also need to have the Tkinter library, which comes preinstalled with standard Python distributions.

## How to Run

1. Clone the repository or download the python script.
2. Run the python script using a Python interpreter.

```bash
python sorting_visualizer.py

```
or you can use the .exe file inside the dist directory for windows.

## How to Use
1. Enter the size of the data set you want to sort (between 1 and 10).
2. Choose the sorting speed (between 1 and 10).
3. Select the visualization type (Scatter, Bar, Stem).
4. Choose the sorting algorithm (Selection, Bubble, Insertion, Merge, Quick).
5. Click on "Create" to generate a new random data set.
6. Click on "Start" to begin the sorting process.
7. Click on "Stop" to pause the sorting process.
8. Click on "Reset" to clear the data.
